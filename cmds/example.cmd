require afg3000

# streamDevice protocol file location
epicsEnvSet("STREAM_PROTOCOL_PATH","$(afg3000_DB)")

#DB include for database concatenation
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(afg3000_DB)")

# Device configurations
epicsEnvSet("AFG_IP",    "$(AFG3000_IP=172.30.150.204)")   # Choose afg ethernet address
epicsEnvSet("AFG_PREFIX", "$(AFG3000_PREFIX=AFG3102C)") # Choose site prefix name
epicsEnvSet("AFG_ASYN_PORT",  "AFG3102C")  # Choose asyn port name
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", 100000)

# Setup IOC->hardware link
vxi11Configure("$(AFG_ASYN_PORT)", "$(AFG_IP)", 0, 0.0, "inst0", 0)

# Load records
dbLoadRecords("$(afg3000_DB)/AFG3000Device.template", "P=$(AFG_PREFIX):,PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")
dbLoadRecords("$(afg3000_DB)/AFG3000Channel.template", "P=$(AFG_PREFIX):, R=AO0:, CHANNEL=1, PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")
dbLoadRecords("$(afg3000_DB)/AFG3000Channel.template", "P=$(AFG_PREFIX):, R=AO1:, CHANNEL=2, PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")
dbLoadRecords("$(afg3000_DB)/AFG3000Channel-internal.template", "P=$(AFG_PREFIX):, R=CH0:, CHANNEL=3, PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")
dbLoadRecords("$(afg3000_DB)/AFG3000Channel-internal.template", "P=$(AFG_PREFIX):, R=CH1:, CHANNEL=4, PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")

# Enable asyn trace
#asynSetTraceMask($(AFG_ASYN_PORT),0, 0x9)
#asynSetTraceIOMask($(AFG_ASYN_PORT),0, 0x2)

# Start IOC
iocInit()
